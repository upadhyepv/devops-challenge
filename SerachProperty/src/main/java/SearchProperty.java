import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.iterable.S3Objects;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.apache.commons.io.IOUtils;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class SearchProperty {


    public static void main(String[] args) throws Exception {
        S3Access s3Access = new S3Access();
        AmazonS3 s3client= s3Access.getS3Access();
        List<com.amazonaws.services.s3.model.Bucket> buckets = s3client.listBuckets();
        String bucketName="";
        //search bucket containing properties as there can be other buckets not relevant to us
        for (Bucket bucket : buckets) {
            if(bucket.getName().contains("properties"))
                bucketName=bucket.getName();
        }
        //The nucket name which contains properties where we will search for address
        System.out.println("The following bucket contains properties: " + bucketName);


        // reads all objects in Bucket and search for addresses given in  arguments of main
        String finalBucketName = bucketName;
        S3Objects.inBucket(s3client, bucketName).forEach((S3ObjectSummary objectSummary) -> {
            String properpertyKey=objectSummary.getKey();
            S3Object object = s3client.getObject(new GetObjectRequest(finalBucketName, properpertyKey));
            InputStream objectData = object.getObjectContent();
            String textFileContent = null;
            try {
                textFileContent = IOUtils.toString(objectData, StandardCharsets.UTF_8);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            //read adresses provided in argument of main function
            for (String addresstoSearch : args) {
                if(textFileContent.contains(addresstoSearch)){
                    System.out.println("Given address "+ addresstoSearch+ " found in "+ properpertyKey);
                }
            }
            //print the content of text file
            System.out.println("----------------------------------");
            System.out.println(properpertyKey);
            System.out.println("----------------------------------");
            System.out.println(textFileContent);
            System.out.println("----------------------------------");
        });



    }

}
