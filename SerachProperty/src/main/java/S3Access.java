import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class S3Access {

      public AmazonS3 getS3Access() throws Exception {

          File file = new File("src/main/resources/config.properties");
          InputStream input = new FileInputStream(file);

          Properties prop = new Properties();

          // load a properties file
          prop.load(input);
          KeySecurity keySecurity= new KeySecurity();
          //  keys for access.using this keys one can access bucket in my account for next 15days
          // keys are place in config.properties in encrypted form
          String awsAccessKey = keySecurity.decrypteKey(prop.getProperty("s3.awsAccessKey"));
          String awsSecretKey = keySecurity.decrypteKey(prop.getProperty("s3.awsSecretKey"));
          AWSCredentials credentials = new BasicAWSCredentials(
                  awsAccessKey,
                  awsSecretKey
          );
          // create a connection to eu central region as bucket present in this region
          AmazonS3 s3client = AmazonS3ClientBuilder
                  .standard()
                  .withCredentials(new AWSStaticCredentialsProvider(credentials))
                  .withRegion(Regions.EU_CENTRAL_1)
                  .build();
          return s3client;
      }

}
