Please Open SearchProperty in Intellij because relative path are used accordingly.
The main function is SearchProperty.main.

To **run assignment** see below screen shot..the main fuction requires the String which will be searched in S3 Bucket
![Alt text](./HowToRunProgramm.JPG)

If you build project locally and exactly run the main class with argument _'GurlittStrasse'_
then **output** looks like below
![Alt text](./Output.JPG)

Please note that access keys to refer my S3bucket created for this assignment is encrypted and placed in** Config.properties**.
ON AWS S3 Bucket looks like below
![Alt text](./AWS-S3-Bucket.JPG)

